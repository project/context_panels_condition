<?php

/**
 * @file
 * Provides a condition for the Context module if a Panel variant is rendered.
 */

/**
 * Allows selection of a Panels-variation.
 */
class context_panels_condition extends context_condition {

  // Return a list with all Panels variations.
  function condition_values() {


    // Render the list.
    $variations = [];
    $handlers = $this->get_page_manager_handlers();

    foreach ($handlers as $machine_name => $handler) {
      $variations[$machine_name] = $handler->task . ' - ' . $handler->conf['title'];
    }

    return $variations;
  }

  // Tests if the current handler matches the condition configuration.
  function execute($handler) {
    foreach ($this->get_contexts($handler->name) as $context) {
      $this->condition_met($context, $handler->name);
    }
  }

  // Return all page_manager handlers.
  protected function get_page_manager_handlers() {
    // @see http://www.patrickjwaters.com/blog/2014-09-23/how-programmatically-load-panel-pages-database-and-include-panel-pages-stored-code-u

    ctools_include('page', 'page_manager', 'plugins/tasks');
    ctools_include('page_manager.admin', 'page_manager', '');
    ctools_include('export');
    $pages = [];

    $tasks = page_manager_get_tasks_by_type('page');
    $page_types = array();
    foreach ($tasks as $task) {
      if ($task_pages = page_manager_load_task_handlers($task)) {
        $pages = array_merge($pages, $task_pages);
      }
      if ($subtasks = page_manager_get_task_subtasks($task)) {
        foreach ($subtasks as $subtask) {
          $task_pages = page_manager_load_task_handlers($subtask);
          $pages = array_merge($pages, $task_pages);
        }
      }
    }

    // Not all display objects are loaded, make sure to load them.
    foreach ($page_types as &$pages) {
      foreach ($pages as &$page) {
        if (empty($page->conf['display']) && !empty($page->conf['did'])) {
          $page->conf['display'] = panels_load_display($page->conf['did']);
        }
      }
    }

    return $pages;
  }

}
